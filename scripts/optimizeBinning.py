#!/usr/bin/env python

import argparse
import json
import os
import random
import itertools
import numpy as np

import ROOT as R
R.gROOT.SetBatch(True)

import HistogramTools as HT

def find_first_nonempty_bin(fineHist):
    # Find first bin with non-zero content
    startBin = 1
    fineNBins = fineHist.GetNbinsX()
    for i in range(1, fineNBins + 1):
        if fineHist.GetBinContent(i) == 0:
            startBin += 1
        else:
            break
    return fineNBins, startBin
    
def fineBinMerging(startBin, fineNBins, maxUncertainty, binContents, binSumW2, acceptLargerOverFlowUncert ):
    finalBins = [startBin] # array [i j k ...] -> bin 1 in new histo = bins i ... j-1 in fine one, etc.
    uncertainties = []
    upEdge = startBin
    previous_content = 0
    previous_sumw2 = 0
    mergeLastBin = False
    while upEdge <= fineNBins + 1:
        uncertainty = 99999
        content = 0
        sumw2 = 0
        while uncertainty > maxUncertainty:
            if upEdge == fineNBins + 2:
                # we've now included the overflow bin without going below the required uncertainty
                break
            if upEdge == fineNBins + 1:
                # -> stop and make sure we merge last bin with next-to-last one
                mergeLastBin = True
            content += binContents[upEdge]
            sumw2 += binSumW2[upEdge]
            if content != 0:
                uncertainty = np.sqrt(sumw2) / content
            if mergeLastBin and acceptLargerOverFlowUncert:
                del uncertainties[-2]
                uncertainty = np.sqrt(sumw2 + previous_sumw2) / (content + previous_content)
            upEdge += 1
        previous_content = content
        previous_sumw2 = sumw2
        finalBins.append(upEdge)
        uncertainties.append(uncertainty)
    if mergeLastBin and acceptLargerOverFlowUncert:
        del finalBins[-2]
    return finalBins, uncertainties

#Based on uncertainty
def optimizeBinning_unc(fineHist, maxUncertainty, acceptLargerOverFlowUncert=True):
    """ Optimize binning, return result in the form of:
        (list of edges, list of bin numbers of original histo)
        args:
            - acceptLargerOFlowUncert: if false, will always merge overflow with previous bin
                                        to ensure the overflow uncertainty is below the threshold
    """
    fineNBins, startBin = find_first_nonempty_bin(fineHist)
    #print(f"New start bin: {startBin}")

    binContents = list(fineHist)
    binSumW2 = list(fineHist.GetSumw2())
    
    finalBins, uncertainties = fineBinMerging(startBin, fineNBins, maxUncertainty, binContents, binSumW2, acceptLargerOverFlowUncert )
    # We now have the new binning. Find the lower edges it corresponds to.
    # The last bin will correspond to the overflow bin so we don't include it explicitly in the edges
    newEdges = []
    for i in finalBins[:-1]:
        newEdges.append(fineHist.GetXaxis().GetBinLowEdge(i))

    print("New bin num:")
    print(len(finalBins) - 1)
    
    return newEdges, finalBins, uncertainties

#Based on bin number
def optimizeBinning_bins(fineHist, binNum, acceptLargerOverFlowUncert=True):
    """ Optimize binning, return result in the form of:
        (list of edges, list of bin numbers of original histo)
        args:
            - acceptLargerOFlowUncert: if false, will always merge overflow with previous bin
                                        to ensure the overflow uncertainty is below the threshold
    """
    fineNBins, startBin = find_first_nonempty_bin(fineHist)
    #print(f"New start bin: {startBin}")

    binContents = list(fineHist)
    binSumW2 = list(fineHist.GetSumw2())
    
    totalUncertainty = np.sqrt(sum(binSumW2)) / sum(binContents)
    
    maxUncertainty = totalUncertainty * np.sqrt( binNum + 1 )
    
    finalBins = [startBin] # array [i j k ...] -> bin 1 in new histo = bins i ... j-1 in fine one, etc.
    while len(finalBins) != binNum + 1:
        finalBins, uncertainties = fineBinMerging(startBin, fineNBins, maxUncertainty, binContents, binSumW2, acceptLargerOverFlowUncert )
            
        if len(finalBins) == binNum + 1:
            continue
        elif len(finalBins) < binNum + 1:
            maxUncertainty += 0.00005
        elif len(finalBins) > binNum + 1:
            maxUncertainty -= 0.000025

    # We now have the new binning. Find the lower edges it corresponds to.
    # The last bin will correspond to the overflow bin so we don't include it explicitly in the edges
    newEdges = []
    for i in finalBins[:-1]:
        newEdges.append(fineHist.GetXaxis().GetBinLowEdge(i))

    print("Maximum Uncertainty:")
    print(maxUncertainty)
    
    return newEdges, finalBins, uncertainties



def rebinCustom(hist, binning, name):
    """ Rebin 2D hist using bin numbers and edges as returned by optimizeBinning() """

    edges = binning[0]
    nBins = len(edges) - 1
    oldNbins = hist.GetNbinsX()
    newHist = R.TH2D(name, hist.GetTitle(), nBins, np.array(edges), nBins, np.array(edges))
    newHist.Sumw2()
    oldSumw2 = list(hist.GetSumw2())

    # we fill the overflow bins properly but not the underflows
    for newBinX in range(1, nBins + 2):
        for newBinY in range(1, nBins + 2):
            content = 0
            sumw2 = 0
            maxOldBinX = binning[1][newBinX] if newBinX <= nBins else oldNbins + 2
            for oldBinX in range(binning[1][newBinX - 1], maxOldBinX):
                #maxOldBinY = binning[1][newBinY] if newBinY <= nBins else oldNbins + 2
                #for oldBinY in range(binning[1][newBinY - 1], maxOldBinY):
                    #content += hist.GetBinContent(oldBinX, oldBinY)
                    #sumw2 += oldSumw2[hist.GetBin(oldBinX, oldBinY)]
                    content += hist.GetBinContent(oldBinX)
                    sumw2 += oldSumw2[hist.GetBin(oldBinX)]
            newHist.SetBinContent(newBinX, newBinY, content)
            newHist.GetSumw2()[newHist.GetBin(newBinX, newBinY)] = sumw2

    return newHist
    
def plotBinning(binning, name, folder):
    style = HT.setTDRStyle()
    style.SetMarkerStyle(8)
    c = R.TCanvas("c", "c")
    edges = binning[0]
    edges.append(edges[len(edges) - 1]*2)
    centers = []
    widths = []
    for idx, edge in enumerate(edges):
        if idx < len(edges) - 1:
            centers.append((edge + edges[idx + 1])/2)
            widths.append((edges[idx + 1] - edge)/2)
    uncertainties = binning[2]
    binning_graph = R.TGraphErrors(len(uncertainties),np.array(centers),np.array(uncertainties),np.array(widths))
    binning_graph.SetLineWidth(2)
    binning_graph.SetLineColor(R.TColor.GetColor("#7044f1"))
    binning_graph.GetYaxis().SetLabelSize(0.03)
    binning_graph.GetYaxis().SetTitleSize(0.03)
    binning_graph.GetYaxis().SetTitleOffset(1.7)
    binning_graph.GetYaxis().SetTitle("Uncertainty")
    binning_graph.GetXaxis().SetTitle("Reco " + name)
    binning_graph.GetXaxis().SetLabelSize(0.03)
    binning_graph.GetXaxis().SetTitleSize(0.03)
    binning_graph.GetXaxis().SetTitleOffset(1.5)
    histMax = -100
    for i in range(1, binning_graph.GetN() + 2):
        histMax = max(histMax, binning_graph.GetPointY(i))
        
    binning_graph.GetYaxis().SetRangeUser(0, histMax * 1.6)
    binning_graph.Draw("ALP")
    c.SaveAs(os.path.join(folder, name + ".pdf"))
    

def plotSlices(hist, name, folder, norm=True):
    """ Plot gen-slices from 2D reco vs. gen hist """

    style = HT.setTDRStyle()
    style.SetMarkerStyle(0)

    c = R.TCanvas("c", "c")

    colors = itertools.cycle([
        "#7044f1",
        "#45ad95",
        "#c07f00",
        "#da7fb7",
        "#ff2719",
        "#251c00"]
    )

    nBins = hist.GetNbinsX()
    projs = []
    legEntries = []
    # get all projections in the reco axis including the gen overflow bin
    for xBin in range(1, nBins + 2):
        projs.append(hist.ProjectionY(name + str(xBin), xBin, xBin))
        upEdge = hist.GetXaxis().GetBinLowEdge(xBin+1) if xBin <= nBins else -1
        legEntries.append(f"Gen {name} from {hist.GetXaxis().GetBinLowEdge(xBin):.2f} to {upEdge:.2f}")

    projs[0].SetLineWidth(2)
    projs[0].SetLineStyle(2)
    projs[0].SetLineColor(R.TColor.GetColor(next(colors)))
    projs[0].GetYaxis().SetLabelSize(0.03)
    projs[0].GetYaxis().SetTitleSize(0.03)
    projs[0].GetYaxis().SetTitleOffset(1.7)
    # projs[0].GetYaxis().SetTitle("Events")
    projs[0].GetXaxis().SetTitle("Reco " + name)
    projs[0].GetXaxis().SetLabelSize(0.03)
    projs[0].GetXaxis().SetTitleSize(0.03)
    # projs[0].GetXaxis().SetLabelOffset(0.05)
    projs[0].GetXaxis().SetTitleOffset(1.5)
    if norm:
        # include the overflow in the integrals
        projs[0].Scale(1./projs[0].Integral(1, nBins + 1))
    projs[0].Draw("Lhist")
    projs[0].Draw("E0same")

    for i, proj in enumerate(projs[1:]):
        proj.SetLineWidth(2)
        proj.SetLineColor(R.TColor.GetColor(next(colors)))
        proj.SetLineStyle(2)
        if norm:
            proj.Scale(1./proj.Integral(1, nBins + 1))
        proj.Draw("Lhistsame")
        proj.Draw("E0same")
        # make sure we also draw the overflow bin
        proj.GetXaxis().SetRange(1, nBins + 1)
    
    histMax = -100
    histMin = 9999999
    for i in range(1, projs[0].GetNbinsX() + 2):
        histMax = max(histMax, *[h.GetBinContent(i) for h in projs])
        histMin = min(histMin, *[h.GetBinContent(i) for h in projs])
    projs[0].GetYaxis().SetRangeUser(0, histMax * 1.6)
    projs[0].GetXaxis().SetRange(1, nBins + 1)

    l = R.TLegend(0.53, 0.65, 0.98, 0.92)
    l.SetTextFont(42)
    l.SetFillColor(R.kWhite)
    l.SetFillStyle(0)
    l.SetBorderSize(0)

    for i in range(len(projs)):
        l.AddEntry(projs[i], legEntries[i])
    l.Draw("same")

    c.SaveAs(os.path.join(folder, name + ".pdf"))


def binRound(binEdge, hist_name):
    base = 5
    integer_vars = ["_pt","_HT","_M"]
    decimal_vars = ["_eta","_dPhi","_DR"]
    small_bin_vars = ["1lep_6j_4b_extra_jet2_pt","1lep_5j_3b_bjet3_pt","1lep_6j_4b_bjet4_pt","1lep_6j_4b_bjet3_pt","1lep_6j_4b_extra_jet_M"]
    if any(x in hist_name for x in small_bin_vars):
        base = 1
    if any(x in hist_name for x in integer_vars):
        digits = 0
    elif any(x in hist_name for x in decimal_vars):
        digits = 2
    return round(base * round(binEdge/base, digits),2)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='input file', required=True)
    parser.add_argument('-o', '--output', help='json output file name for new binning')
    parser.add_argument('-f', '--folder', help='output folder for plots', required=True)
    parser.add_argument('-u', '--uncertainty', type=float, help='max stat. uncertainty')
    parser.add_argument('-b', '--bins', type=float, help='desired number of bins')
    args = parser.parse_args()
    #eras = ["2016ULpreVFP", "2016ULpostVFP", "2017UL", "2018UL", "run2"]
    eras = ["run2"]
    
    for era in eras:
        if not os.path.isdir(args.folder+"_"+era+"_rebin_plots"):
            os.makedirs(args.folder+"_"+era+"_rebin_plots")
        if not os.path.isdir(args.folder+"_"+era+"_uncertainty_plots"):
            os.makedirs(args.folder+"_"+era+"_uncertainty_plots")
        inFile = HT.openFileAndGet(args.input+"_"+era+".root")
        hists = dict()
        HT.readRecursiveDirContent(hists, inFile, resetDir=False)
        outFile = HT.openFileAndGet(os.path.join(args.folder+"_"+era+"_rebin_plots", "rebinned.root"), "recreate")
        binnings = {}
        #for obs in observables:
        for hist in hists:
            new_hist_name = hist.replace("fineBinning_","")
            if "fineBinning_" in hist:
                print(f"Doing {hist}")
                fineHist = hists[hist]
                if args.uncertainty:
                    binning = optimizeBinning_unc(fineHist, args.uncertainty)
                elif args.bins:
                    binning = optimizeBinning_bins(fineHist, args.bins)
                binnings[new_hist_name] = binning[0]
                binnings[new_hist_name] = [binRound(binEdge, new_hist_name) for binEdge in binnings[new_hist_name]]
                newHist = rebinCustom(fineHist, binning, fineHist.GetName() + "_rebin")
                plotBinning(binning, new_hist_name, args.folder+"_"+era+"_uncertainty_plots")
                outFile.cd()
                newHist.Write()
                #plotSlices(newHist, new_hist_name, args.folder+"_"+era+"_rebin_plots",  norm=False )

            if args.output is not None:
                with open(args.output+"_"+era+"_bin.json", 'w') as _f:
                    json.dump(binnings, _f)
        inFile.Close()
        outFile.Close()
