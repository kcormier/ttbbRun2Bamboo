from bamboo.plots import Plot, SummedPlot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo.plots import VariableBinning as VarBin
from bamboo import treefunctions as op

import definitions as defs
import utils

import copy
import numpy as np

def split_bins_in_two( edges ):

    np_edges = np.array(edges)
    new_edges = (np_edges[:-1] + np_edges[1:])/2
    all_edges = np.concatenate( [np_edges, new_edges])
    all_edges.sort()

    #return as list, since currently building from
    #lists in actual use
    return list(all_edges)

def join_bins( edges ):
    new_edges = []
    if len(edges) % 2 == 1:
        new_edges = edges[::2]
    else:
        raise ValueError('An odd number of reco bins was found. Bin merging for reco bins now unclear.')
    return new_edges

class Observable():
    '''Associate naming, binning, expressions for a given Observable into one class with some
        convenience functions'''

    def __init__(self, collection=None, secondary_collection=None, minIndex=0, maxIndex=0, tag='', is_gen=False):
        self._collection = collection
        self._secondary_collection = secondary_collection
        self._index = minIndex
        self._min_index = minIndex
        self._max_index = maxIndex
        self._started_iter = False
        self._tag = tag
        self.is_gen = is_gen  # whether the observable instance is considered at gen or reco level
        self.bin_dct = {}  # will hold 'reco' and 'gen' binnings

    def expression(self, i=None):
        if self._collection is None:
            raise ValueError(f'The value of an Observable expression whose collection is not set is undefined.')

    def bins(self, ph_space='reco', i=None):
        i = min(self._idx(i), len(self.bin_dct[ph_space])-1 )
        return self.bin_dct[ph_space][i]

    def _simple_name(self, i=None):
        return self.__name__

    def name(self, i=None):
        return f'{self._simple_name()}{self._tag}'

    def pretty_name(self, i=None):
        return self.name(i)

    def ROOT_pretty_name(self, i=None):
        return self.name(i)

    def unit(self):
        pass

    def overflow(self):
        return False

    def logy(self):
        return False

    def plot_opts(self):
        return {}

    #Some functions for iterating over the collection
    #Iteration is used as a convenient way to define e.g. jet_pt for all jets in a collection
    def __iter__(self):
        copy_of_self = copy.deepcopy(self)
        copy_of_self._index = self._min_index
        return copy_of_self

    def __next__(self):
        #No empty observables, always return the 0th index at least
        if not self._started_iter:
            self._started_iter=True
            return self

        if self._index < self._max_index:
            self._index += 1
            return self
        raise StopIteration

    #utility functions
    def _idx(self, i):
        return self._index if i is None else i

class SingleObservable(Observable):
    '''Observable which will only ever have one element'''

    def __init__(self, **kwargs):
        kwargs.update({'maxIndex': 0, 'minIndex': 0})
        super().__init__(**kwargs)

class Jet_Pt(Observable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _gen_bins  = [ [30., 90., 120., 150., 180., 240., 530. ],
                            [30., 70.,  90., 110., 130., 170., 430. ],
                            [30., 60.,  70.,  80.,  90., 120., 330. ],
                            [30., 45.,  55.,  65.,  75.,  90., 230. ],
                            [30., 40.,  50.,  60.,  70., 180. ],
                            [30., 40.,  50.,  60., 130. ] ]
        _reco_bins = [ split_bins_in_two( gen_edges ) for gen_edges in _gen_bins ]
        self.bin_dct['gen']  = [ VarBin(bin_edges) for bin_edges in _gen_bins  ]
        self.bin_dct['reco'] = [ VarBin(bin_edges) for bin_edges in _reco_bins ]

    def expression(self, i=None):
        super().expression()
        return self._collection[self._idx(i)].pt

    def _simple_name(self, i=None):
        return  f'jet{self._idx(i)+1}_pt'

    def pretty_name(self, i=None):
        return r'$p_{\mathrm{T}}(\mathrm{j}_{' + str(self._idx(i)+1) + r'})$'

    def ROOT_pretty_name(self, i=None):
        return r'p_{T}(j_{' + str(self._idx(i)+1) + r'})'

    def unit(self):
        return 'GeV'

    def overflow(self):
        return True

    def logy(self):
        return True


class Jet_AbsEta(Observable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins = [ list(np.linspace(0, 2.04, 13)) + [2.22, 2.4] for i in range(2) ]
        _gen_bins = [ join_bins(bins) for bins in _reco_bins ]
        self.bin_dct['gen']  = [ VarBin(bin_edges) for bin_edges in _gen_bins  ]
        self.bin_dct['reco'] = [ VarBin(bin_edges) for bin_edges in _reco_bins ]

    def expression(self, i=None):
        super().expression()
        return op.abs(self._collection[self._idx(i)].eta)

    def _simple_name(self, i=None):
        return f'jet{self._idx(i)+1}_abseta'

    def pretty_name(self, i=None):
        return r'$|\eta(\mathrm{j}_{ ' + str(self._idx(i)+1) + r'})|$'

    def ROOT_pretty_name(self, i=None):
        return r'|#eta(j_{ ' + str(self._idx(i)+1) + r'})|'

    def unit(self):
        return ''

class Jet_Ht(SingleObservable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins  = [ 140, 300, 350, 400, 450, 500, 550, 600, 650, 720, 800, 900, 1200 ]
        _gen_bins = join_bins(_reco_bins)
        self.bin_dct['gen']  = [ VarBin(_gen_bins) ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]

    def expression(self, i=None):
        return op.rng_sum(self._collection, lambda jet : jet.pt)

    def _simple_name(self, i=None):
        return  'jet_Ht'

    def pretty_name(self, i=None):
        return r"$H^{\mathrm{j}}_{\mathrm{T}}$"

    def ROOT_pretty_name(self, i=None):
        return r"H^{j}_{T}"

    def unit(self):
        return 'GeV'

    def overflow(self):
        return True

class Jet_dPhi(SingleObservable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins  = [ 0., 0.25, 0.5, 0.75, 1., 1.25, 1.5, 1.75, 2., 2.25, 2.5, 2.75, 3.14 ]
        _gen_bins = join_bins(_reco_bins)
        self.bin_dct['gen']  = [ VarBin(_gen_bins) ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]

    def expression(self, i=None):
        return op.abs(op.deltaPhi(self._collection[0].p4, self._secondary_collection[0].p4))

    def _simple_name(self, i=None):
        return  'dPhi_lj_bsoft'

    def pretty_name(self, i=None):
        return r'$|\Delta\phi(\mathrm{lj}^{\mathrm{extra}}_{1},\mathrm{b}_{\mathrm{soft}})|$'

    def ROOT_pretty_name(self, i=None):
        return r'|#Delta#phi(lj^{extra}_{1},b_{soft})|'

class Njet(SingleObservable):
    def __init__(self, nMin=5, nMax=10, **kwargs):
        super().__init__(**kwargs)
        self.bin_dct['gen']  = [ EqBin(nMax-nMin, nMin, nMax) ]
        self.bin_dct['reco'] = [ EqBin(nMax-nMin, nMin, nMax) ]

    def expression(self, nMin=6, i=None):
        return op.rng_len(self._collection)

    def _simple_name(self, i=None):
        return 'Njets'

    def pretty_name(self, i=None):
        return r'$N_{\mathrm{jets}}$'

    def ROOT_pretty_name(self, i=None):
        return r'N_{jets}'

    def overflow(self):
        return True

class Extra_Jet_Pt(Jet_Pt):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins = [ [ 30, 40, 50, 65, 80, 100, 120, 140, 170, 190, 220, 260, 300, 400, 500 ],
                       [ 30, 37, 45, 55, 65, 75, 90, 105, 120, 140, 170 ] ]
        _gen_bins  = [ [ 25,     50,     80,      120,      170,      220,      300,      500 ],
                       [ 25,     45,     65,     90,      120,      170 ] ]
        self.bin_dct['gen'] = [ VarBin(bin_edges) for bin_edges in _gen_bins  ]
        self.bin_dct['reco'] = [ VarBin(bin_edges) for bin_edges in _reco_bins ]

    def _simple_name(self, i=None):
        return f'extra_jet{self._idx(i)+1}_pt'

    def pretty_name(self, i=None):
        return r'$p_{\mathrm{T}}(\mathrm{b}^{\mathrm{extra}}_{' + str(self._idx(i)+1) + r'})$'

    def ROOT_pretty_name(self, i=None):
        return r'p_{T}(b^{extra}_{' + str(self._idx(i)+1) + r'})'

class Extra_Jet_AbsEta(Jet_AbsEta):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _simple_name(self, i=None):
        return f'extra_jet{self._idx(i)+1}_abseta'

    def pretty_name(self, i=None):
        return r'$|\eta(\mathrm{b}^{\mathrm{extra}}_{' + str(self._idx(i)+1) + r'})|$'

    def ROOT_pretty_name(self, i=None):
        return r'|#eta(b^{extra}_{' + str(self._idx(i)+1) + r'})|'

class Extra_Jet_dR(SingleObservable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins = [ 0.3, 0.5, 0.55, 0.62, 0.7, 0.77, 0.85, 0.92, 1., 1.1, 1.2, 1.35, 1.5, 2., 3.5 ]
        _gen_bins = join_bins(_reco_bins)
        self.bin_dct['gen' ] = [ VarBin(_gen_bins) ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]

    def expression(self):
        return op.deltaR(self._collection[0].p4, self._collection[1].p4)

    def _simple_name(self):
        return 'extra_jet_DR'

    def pretty_name(self):
        return r'$\Delta\mathrm{R}(\mathrm{b}\mathrm{b}^{\mathrm{extra}})$'

    def ROOT_pretty_name(self):
        return r'#Delta R(bb^{extra})'

class Extra_Jet_Mass(SingleObservable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins  = [ 10, 30, 40, 55, 70, 80, 90, 105, 120, 140, 170, 210, 250 ]
        _gen_bins = join_bins(_reco_bins)
        self.bin_dct['gen' ] = [ VarBin( _gen_bins)  ]
        self.bin_dct['reco'] = [ VarBin( _reco_bins) ]

    def expression(self):
        return op.invariant_mass(self._collection[0].p4, self._collection[1].p4)

    def _simple_name(self):
        return 'extra_jet_M'

    def pretty_name(self):
        return r'$\mathrm{m}(\mathrm{b}\mathrm{b}^{\mathrm{extra}})$'

    def ROOT_pretty_name(self):
        return r'm(bb^{extra})'

    def unit(self):
        return 'GeV'

    def overflow(self):
        return True

class Extra_Jet_Sum_Pt(SingleObservable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins  = [ 20, 60, 80, 100, 130, 155, 180, 205, 230, 260, 300, 350, 400 ]
        _gen_bins = join_bins(_reco_bins)
        self.bin_dct['gen' ] = [ VarBin(_gen_bins)  ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]

    def expression(self):
        return  (self._collection[0].p4 + self._collection[1].p4).Pt()

    def _simple_name(self):
        return 'extra_jet_pt'

    def pretty_name(self):
        return r'$p_{\mathrm{T}}(\mathrm{b}\mathrm{b}^{\mathrm{extra}})$'

    def ROOT_pretty_name(self):
        return r'p_{T}(bb^{extra})'

    def unit(self):
        return 'GeV'

    def overflow(self):
        return True

class Extra_Jet_Sum_AbsEta(SingleObservable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins = list(np.linspace(0, 2.4, 13))
        _gen_bins = join_bins(_reco_bins)
        self.bin_dct['gen']  = [ VarBin(_gen_bins)  ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]

    def expression(self):
        return op.abs((self._collection[0].p4 + self._collection[1].p4).eta())

    def _simple_name(self):
        return 'extra_jet_abseta'

    def pretty_name(self):
        return r'$|\eta|(\mathrm{b}\mathrm{b}^{\mathrm{extra}})$'

    def ROOT_pretty_name(self):
        return r'|#eta|(bb^{extra})'

    # there is no cut on eta(bb) so it could potentially go higher
    def overflow(self):
        return True

class Max_Mbb(SingleObservable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins  = [ 30, 115, 140, 180, 210, 240, 280, 310, 350, 420, 500, 600, 800 ]
        _gen_bins = join_bins(_reco_bins)
        self.bin_dct['gen']  = [ VarBin(_gen_bins)  ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]

    def expression(self):
        return op.invariant_mass(self._collection[0].p4, self._collection[1].p4)

    def _simple_name(self):
        return 'largest_Mbb'

    def pretty_name(self):
        return r'$\mathrm{m}_{\mathrm{b}\mathrm{b}}^{\mathrm{max}}$'

    def ROOT_pretty_name(self):
        return r'm_{bb}^{max}'

    def unit(self):
        return 'GeV'

    def overflow(self):
        return True

class Average_DR(SingleObservable):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins = [ 0.6, 1.4, 1.6, 1.75, 1.9, 2., 2.1, 2.2, 2.3, 2.4, 2.5, 2.65, 2.8, 3., 3.5 ]
        _gen_bins = join_bins(_reco_bins)
        self.bin_dct['gen' ] = [ VarBin(_gen_bins) ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]

    def expression(self):
        return op.rng_mean(op.map(self._collection, lambda p: op.deltaR(p[0].p4, p[1].p4)))

    def _simple_name(self):
        return 'average_DR'

    def pretty_name(self):
        return r'$\Delta\mathrm{R}_{\mathrm{b}\mathrm{b}}^{\mathrm{avg}}$'

    def ROOT_pretty_name(self):
        return r'#Delta R_{bb}^{avg}'

class Bjet_Pt(Jet_Pt):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
    def _simple_name(self, i=None):
        return f'bjet{self._idx(i)+1}_pt'
    def pretty_name(self, i=None):
        return r'$p_{\mathrm{T}}(\mathrm{b}_{' + str(self._idx(i)+1) + r'})$'
    def ROOT_pretty_name(self, i=None):
        return r'p_{T}(b_{' + str(self._idx(i)+1) + r'})'
class Bjet_Pt_5j_3b(Bjet_Pt):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins = [
            [],  # 1st not measured
            [],  # 2nd not measured
            [ 25, 40, 60, 80, 100, 125, 150, 175, 200 ] ]
        _gen_bins = [[], [],
            [ 25,     60,     100,      150,      200 ] ]
        self.bin_dct['gen']  = [ VarBin( bin_edges) for bin_edges in _gen_bins  ]
        self.bin_dct['reco'] = [ VarBin( bin_edges) for bin_edges in _reco_bins ]
class Bjet_Pt_6j_4b(Bjet_Pt):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins = [
            [],  # 1st not measured
            [],  # 2nd not measured
            [ 30, 40, 50, 60, 80, 100, 120, 140, 160, 180, 200 ],
            [ 30, 35, 40, 50, 60, 70, 80, 90, 100, 110, 120 ] ]
        _gen_bins = [[], [],
            [ 25,     50,     80,      120,      160,      200 ],
            [ 25,     40,     60,     80,     100,      120 ] ]
        self.bin_dct['gen']  = [ VarBin( bin_edges) for bin_edges in _gen_bins  ]
        self.bin_dct['reco'] = [ VarBin( bin_edges) for bin_edges in _reco_bins ]


class Bjet_AbsEta(Jet_AbsEta):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _simple_name(self, i=None):
        return f'bjet{self._idx(i)+1}_abseta'

    def pretty_name(self, i=None):
        return r'$|\eta(\mathrm{b}_{' + str(self._idx(i)+1) + r'})|$'

    def ROOT_pretty_name(self, i=None):
        return r'|#eta(b_{' + str(self._idx(i)+1) + r'})|'

class bJet_Ht(Jet_Ht):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
    def _simple_name(self, i=None):
        return 'bJet_Ht'
    def pretty_name(self, i=None):
        return r"$H^{\mathrm{b}}_{\mathrm{T}}$"
    def ROOT_pretty_name(self, i=None):
        return r"H^{b}_{T}"
class bJet_Ht_5j_3b(bJet_Ht):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins = [ 90, 130, 150, 200, 250, 300, 350, 400, 450, 520, 600, 670, 750, 820, 900 ]
        _gen_bins = join_bins(_reco_bins)
        _gen_bins[0] = 75
        self.bin_dct['gen']  = [ VarBin(_gen_bins) ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]
class bJet_Ht_6j_4b(bJet_Ht):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins = [ 120, 200, 250, 300, 350, 400, 450, 520, 600, 670, 750, 850, 1000]
        _gen_bins = join_bins(_reco_bins)
        _gen_bins[0] = 100
        self.bin_dct['gen']  = [ VarBin(_gen_bins) ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]

class lightJet_Ht(Jet_Ht):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins  = [ 60, 120, 200, 275, 350, 425, 500, 575, 650, 725, 800 ]
        _gen_bins = join_bins(_reco_bins)
        self.bin_dct['gen']  = [ VarBin(_gen_bins) ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]

    def _simple_name(self, i=None):
        return 'lightJet_Ht'

    def pretty_name(self, i=None):
        return r"$H^{\mathrm{light}}_{\mathrm{T}}$"

    def ROOT_pretty_name(self, i=None):
        return r"H^{light}_{T}"

class extraLightJet_Pt(Jet_Pt):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        _reco_bins = [ 30, 40, 60, 80, 100, 120, 150, 170, 200, 250, 300, 350, 400]
        _gen_bins  = [ 25,     60,     100,      150,      200,      300,      400]
        self.bin_dct['gen']  = [ VarBin(_gen_bins)  ]
        self.bin_dct['reco'] = [ VarBin(_reco_bins) ]

    def _simple_name(self, i=None):
        return 'extra_lightJet_pt'

    def pretty_name(self, i=None):
        return r'$p_{\mathrm{T}}(\mathrm{lj}^{\mathrm{extra}}_{1})$'

    def ROOT_pretty_name(self, i=None):
        return r'p_{T}(lj^{extra}_{1})'

class NExtraBJet_AboveWP(Observable):
    pretty_working_points = {'T' : 'tight',
                             'M' : 'medium',
                             'L' : 'loose' }

    def __init__(self, era, tagger='BtagDeepFlavB', working_point='T', max_bin=2, overflow=False, **kwargs):
        super().__init__( **kwargs )
        if not working_point in self.pretty_working_points.keys():
            raise ValueError(f'BTag working point {working_point} not in list of known working points: {self.pretty_working_points}')

        self._tagger = tagger
        self._wp     = working_point
        self._era    = era
        self._cut_value = defs.bTagWorkingPoints[era][tagger][working_point]  if tagger is not None else None
        self._nmax   = max_bin
        self._overflow = overflow
        # this observable is not defined at gen level!
        self.bin_dct['reco'] = [ EqBin(max_bin + 1, 0., max_bin+1) ]

    def expression(self):
        return op.rng_count( self._collection[2:2+self._nmax], lambda bjet: getattr(bjet, self._tagger ) >= self._cut_value )

    def _simple_name(self, i=None):
        return f'n_extra_{self._wp}_tagged_jets'

    def pretty_name(self, i=None):
        return r'$N_{\mathrm{b}}^{' + self._wp + r'}$'

    def ROOT_pretty_name(self, i=None):
        return r'N_{b}^{' + self._wp + r'}'

    def overflow(self):
        return self._overflow

class NBTaggedJets(NExtraBJet_AboveWP):
    def __init__(self, binning, **kwargs):
        super().__init__( **kwargs )
        self.bin_dct['reco'] = [binning]
        self.bin_dct['gen'] = [binning]

    def expression(self):
        # Only here do we need to make the difference between gen and reco level
        # to evaluate the observable's expression
        if self.is_gen:
            return op.rng_count(self._collection, lambda jet: jet.hadronFlavour == 5)
        else:
            return op.rng_count(self._collection, lambda jet: getattr(jet, self._tagger) >= self._cut_value)

    def _simple_name(self, i=None):
        return f'n_{self._wp}_tagged_jets'


#Functions for defining reco and gen plots
def get_expr(obs, plot_type, aux, cr_def):

    if plot_type in ['gen','reco']:
        if aux is None or plot_type == 'gen':
            return obs.expression()
        else:
            return (obs.expression(), aux.expression() )
    elif plot_type in ['migration']:
        if aux is None and cr_def is None:
            return ( obs[0].expression(), obs[1].expression() )
        elif aux is not None and cr_def is None:
            return (obs[0].expression(), obs[1].expression(), aux.expression() )
        elif aux is None and cr_def is not None:
            return (obs[0].expression(), cr_def.expression() )
    else:
        raise ValueError(f'Plot type must be one of "gen", "reco" or "migration", but found {plot_type}')

def get_bins(obs, plot_type, aux, cr_def):
    if plot_type in ['gen','reco']:
        if aux is None or plot_type == 'gen':
            return obs.bins(plot_type)
        else:
            return (obs.bins(plot_type), aux.bins('reco') )
    elif plot_type in ['migration']:
        if aux is None and cr_def is None:
            return ( obs[0].bins('gen'), obs[1].bins('reco') )
        elif aux is not None and cr_def is None:
            return (obs[0].bins('gen'), obs[1].bins('reco'), aux.bins('reco') )
        elif aux is None and cr_def is not None:
            return (obs[0].bins('gen'), cr_def.bins('reco') )
    else:
        raise ValueError(f'Plot type must be one of "gen", "reco" or "migration", but found {plot_type}')

def get_plot_func(plot_type, aux):
    if plot_type in ['gen']:
        return Plot.make1D
    elif plot_type in ['reco']:
        if aux is None:
            return Plot.make1D
        else:
            return Plot.make2D
    elif plot_type in ['migration']:
        if aux is None:
            return Plot.make2D
        else:
            return Plot.make3D
    else:
        raise ValueError(f'Plot type must be one of "gen", "reco" or "migration", but found {plot_type}')

def get_plot_name( obs, sel, plot_type, sel_name=None, obs_delim='_obsis', migration_tag='migrationMatrix'):
    sel_name = sel.name if sel_name is None else sel_name
    if plot_type in ['gen','reco']:
        plot_name  = f'{sel_name}{obs_delim}_{obs.name()}'
        plot_title = f'{obs.pretty_name()} [{obs.unit()}]'
    elif plot_type in ['migration']:
        obs = obs[0]
        plot_name = f'{migration_tag}_{sel_name}{obs_delim}_{obs.name()}'
        plot_title = f'Migration Matrix {obs.pretty_name()} [{obs.unit()}]'
    else:
        raise ValueError(f'Plot type must be one of "gen", "reco" or "migration", but found {plot_type}')
    return (plot_name, plot_title)

def make_plot( obs, sel, plot_type, aux=None, cr_def=None, sel_name=None, obs_delim='_obsis', migration_tag='migrationMatrix'):

    assert(not (aux is not None and cr_def is not None))
    plot_name, plot_title = get_plot_name(obs, sel, plot_type, sel_name)

    expr = get_expr(obs, plot_type, aux, cr_def)
    bins = get_bins(obs, plot_type, aux, cr_def)
    plt_func = get_plot_func(plot_type, aux)

    return (plt_func(plot_name, expr, sel, bins, title=plot_title), plot_name)


def addObservablePlots(observable, selections, collections, secondary_collections=None, obs_delim='_obsis', aux=None, cr_def=None, config=None, config_kwargs=None, obs_kwargs=None):
    '''Define a set of bamboo plots for a single measurement (i.e. unfolded observable). Optionally add their information to a configuration
        if this includes gen-level information (i.e. if it is a signal sample).'''

    config_kwargs = config_kwargs or dict()
    obs_kwargs = obs_kwargs or dict()
    plots = []
    do_reco = selections.has_reco()
    do_gen = selections.has_gen()
    reco_obs = observable( collection = collections.reco_collection, **obs_kwargs) if do_reco else None
    gen_obs  = observable( collection = collections.gen_collection, is_gen=True, **obs_kwargs) if do_gen else None
    if secondary_collections is not None:
        reco_obs = observable( collection = collections.reco_collection, secondary_collection = secondary_collections.reco_collection, **obs_kwargs) if do_reco else None
        gen_obs  = observable( collection = collections.gen_collection, secondary_collection = secondary_collections.gen_collection, is_gen=True, **obs_kwargs) if do_gen else None
    observable_sets = zip(*[ obs for obs in [reco_obs,gen_obs] if obs is not None])

    for obs_iter  in observable_sets:
        if do_reco:
            reco_obs_iter = obs_iter[0]
        if do_gen:
            gen_obs_iter = obs_iter[1 if do_reco else 0]
            gen_plot, gen_plot_name = make_plot(gen_obs_iter, selections.gen_selection, 'gen')
            plots.append(gen_plot)
        reco_names = []
        mig_names = []
        cr_reco_names = []

        if do_reco:
            for sel_iter in selections.detector_level_selections(): #zips reco and migration if migration exists, else just zips reco
                reco_sel = sel_iter[0]
                if do_gen: #Doing gen implies doing the migration as well
                    mig_sel = sel_iter[1]

                # for CR's, there is a SINGLE reco plot, not one per observable -> do not create it here
                if "CR" not in reco_sel.name:
                    reco_plot, reco_name = make_plot( reco_obs_iter, reco_sel, 'reco', aux=aux)
                    plots.append(reco_plot)
                elif not cr_def is None: #still add the plot to the list of reco plots for this unfolding
                    reco_name, _ = get_plot_name(cr_def, reco_sel, 'reco')
                    cr_reco_names += [reco_name]
                reco_names += [ reco_name ]

                if do_gen:
                    if "CR" in mig_sel.name:
                        mig_plot, mig_name = make_plot( [gen_obs_iter, reco_obs_iter], mig_sel, 'migration', cr_def=cr_def, sel_name=reco_sel.name)
                    else:
                        mig_plot, mig_name = make_plot( [gen_obs_iter, reco_obs_iter], mig_sel, 'migration', aux=aux, sel_name=reco_sel.name)
                    plots.append(mig_plot)
                    mig_names += [mig_name]

            if config is not None:
                if gen_obs is not None:
                    plt_names = MeasurementPlots(gen_plot_name, reco_names, mig_names)
                    config.add_measurement( gen_obs_iter, plt_names, **config_kwargs )
                    for cr_plot in cr_reco_names:
                        reco_names.remove(cr_plot)
                    config.add_reco_plots( reco_names, reco_obs_iter, aux )

    return plots


def addAllMeasurements( obs_list, *args, **kwargs):
    plots =[]
    for obs in obs_list:
        meas = Measurement( obs, *args )
        plots += meas.add_plots( **kwargs )
    return plots

class Measurement():
    '''Grouping together the observable, the selections and the object collections that
        define a single "Measurement", i.e. a single unfolded observable.'''

    def __init__(self, observable, selections, collections, secondary_collections=None):
        self.selections = selections
        self.collections = collections
        self.secondary_collections = secondary_collections
        self.observable = observable

    def add_plots(self, **kwargs):
        plots = addObservablePlots( self.observable, self.selections, self.collections, self.secondary_collections,  **kwargs)
        return plots

    def has_gen(self):
        return self.selections.has_gen()


class MeasurementSelection():
    '''Grouping together the generator, reco, and gen+reco, selections that
        define the phase spaces for a given measurement.'''

    def __init__(self):
        self.gen_selection = None
        self.reco_selections = []
        self.gen_and_reco_selections = []
        self.metadata = {}

    def detector_level_selections(self):
        '''zip all selections that have any detector-level component, so they can be iterated over'''
        if len(self.gen_and_reco_selections) == 0:
            return zip(self.reco_selections)
        else:
            return zip(self.reco_selections, self.gen_and_reco_selections)

    def has_gen(self):
        return self.gen_selection is not None

    def has_reco(self):
        return len(self.reco_selections) > 0 or len(self.gen_and_reco_selections) > 0

class MeasurementCollection():
    '''grouping together the generator-level and reco-level objects which are used for a given measurement.'''

    def __init__(self, reco_collection=None, gen_collection=None):
        self.gen_collection  =  gen_collection
        self.reco_collection =  reco_collection

class MeasurementPlots():
    '''Storing the names of generator, reco and migration plots used in a single measurement
        (i.e. unfolded observable)
        Note: This does not store the actual plots, just the name information.'''

    def __init__(self, gen_plot, reco_plots, mig_plots):
        '''gen_plot: a single plot name, reco_plots and mig plots: arrays of reco
        and migration plot names respectively.'''
        self.gen_plot = gen_plot
        self.reco_plots = [ rp for rp in reco_plots ] #make copies so were not affected by changes of the array
        self.mig_plots =  [ mp for mp in mig_plots ]

