import yaml
import os
import numpy as np

from bamboo.plots import EquidistantBinning as EqBin
from bamboo.plots import VariableBinning as VarBin

import utils

def get_bin_array(bins):
    arr =[]
    if type(bins) == EqBin:
        step_size = (bins.maximum - bins.minimum)/bins.N
        epsilon = step_size/2
        arr = list(np.arange(bins.minimum, bins.maximum+epsilon, step_size))

    elif type(bins) == VarBin:
        arr = bins.binEdges
    else:
        raise TypeError(f'Expected bins object of type {EqBin} or {VarBin}, but found {type(bins)}.' )

    return [float(elem) for elem in arr]

class ConfigMismatchError(ValueError):
   pass 

class ConfigWriter():
    '''Stores information about observables and their plot_names, which can then be written to a .yml
       file. The file can then be read in for use in further processing.'''


    #these fixed keys are essentially part of what defines the observable
    #So a single observable must always have consistent values of these properties
    fixed_meas_keys = ["unit", "pretty_name", "ROOT_pretty_name", "overflow", "gen_plot" ]

    def __init__(self,config_name='unfolding_config.yml',plots_name='unfolding_plots.yml'):
        self._config_name = config_name
        self._plots_name = plots_name
        self._meas_dict = {}
        self._reco_plot_dict = {}
        self._aux_dict = {}
        self._cr_dict = {}

    def add_measurement(self, obs, plot_names=None):
        if not obs.name() in self._meas_dict:
            self._meas_dict[obs.name()] = self._new_meas_entry( obs, plot_names )

        else: # some entry already exists, just add to it if necessary
            if plot_names is not None: #nothing to extend if its just the observable
                self._check_meas_for_consistency( obs, plot_names )
                self._extend_meas_entry( obs, plot_names )

    def get_phase_space(self, plotName):
        phaseSpace = plotName.replace("genLevel_lepton_","")
        phaseSpace = phaseSpace.split("_obsis_")[0]
        return phaseSpace


    def get_pretty_phase_space(self, plotName):
        phase_space = self.get_phase_space(plotName)
        # FIXME better way of getting the phase space when adding
        # the measurements, rather than doing guesswork here?
        njs = phase_space.split("_")
        njs = [ nj[0] for nj in njs ]
        if len(njs) == 2:
            return r"$\geq " + njs[0] + \
                    r"$ jets: $\geq " + \
                    njs[1] + r" \mathrm{b}$"
        elif len(njs) == 3:
            return r"$\geq " + njs[0] + \
                    r"$ jets: $\geq " + \
                    njs[1] + r" \mathrm{b}$, " + \
                    r"$\geq " + njs[2] + r"$ light"
        else:
            return phase_space


    def _new_meas_entry(self, obs, plot_names=None):
        entry = {
            "unit"        : obs.unit(),
            "pretty_name" : obs.pretty_name(),
            "ROOT_pretty_name" : obs.ROOT_pretty_name(),
            "overflow"    : obs.overflow(),
            "logy"    : obs.logy(),
            "phase_space" : self.get_phase_space(plot_names.gen_plot),
            "pretty_phase_space" : self.get_pretty_phase_space(plot_names.gen_plot)
        }

        if plot_names is not None:
           entry.update({"gen_plot"     : plot_names.gen_plot,
                         "reco_plots"   : plot_names.reco_plots,
                         "mig_plots"    : plot_names.mig_plots,
                        })
        return entry

    def _check_meas_for_consistency( self, obs, plot_names):   
        existing_entry = self._meas_dict[obs.name()]
        update_entry = self._new_meas_entry(obs, plot_names = plot_names)

        for key in self.fixed_meas_keys:
            existing_val = existing_entry[key]
            new_val      = update_entry[key]
            if not existing_val == new_val:
                raise ConfigMismatchError(f'''Existing entry for observable {obs.name()} conflicts with new entry on key {key}. 
                                            The existing value is {existing_val}, the new value is {new_val}. 
                                            These two values must be the same.''')

    def _extend_meas_entry(self, obs, plot_names):
        for reco_plot, mig_plot in zip(plot_names.reco_plots, plot_names.mig_plots):
            if not mig_plot in self._meas_dict[ obs.name() ]["mig_plots"]:
                self._meas_dict[ obs.name() ]["reco_plots"] += [ reco_plot ]
                self._meas_dict[ obs.name() ]["mig_plots"]  += [ mig_plot ]

    def add_reco_plots( self, reco_plots, obs, aux):
        for plot in reco_plots:
            self.add_reco_plot(plot, obs, aux)

    def add_reco_plot(self, reco_plot_name, obs, aux):
        if not reco_plot_name in self._reco_plot_dict:
            self._reco_plot_dict[reco_plot_name] =self._new_reco_entry( reco_plot_name, obs, aux)

            if (aux is not None) and not (aux.name() in self._aux_dict):
                self.add_aux_entry(aux)

        else: #shouldn't need to 'extend' these entries, do nothing
            pass

    def get_selection(self, plotType, plotName):
        selectionName = plotName.replace(f"{plotType}_","")
        selectionName = selectionName.split("_obsis_")[0]
        return selectionName

    def get_channel(self, plotName):
        if "_1mu_" in plotName:
            lepton = "muon"
        elif "_1ele_" in plotName:
            lepton = "electron"
        return lepton

    def _new_reco_entry(self, reco_plot_name, obs, aux):
        aux_name = aux.name() if aux is not None else None
        entry = { 'aux-name' : aux_name, 
                  'bins'     : get_bin_array(obs.bins('reco')),
                  'overflow' : obs.overflow(),
                  'selection': self.get_selection("reco", reco_plot_name),
                  'channel'  : self.get_channel(reco_plot_name) }

        return entry

    def add_aux_entry(self, aux):
        self._aux_dict[aux.name()] = { 'bins'       : get_bin_array(aux.bins('reco')),
                                       'pretty_name': aux.pretty_name(), 
                                       'ROOT_pretty_name': aux.ROOT_pretty_name(), 
                                       'overflow'   : aux.overflow() }

    def add_cr_entry(self, plot_name, cr_def):
        self._cr_dict[plot_name] = { 'bins'       : get_bin_array(cr_def.bins('reco')),
                                     'pretty_name': cr_def.pretty_name(), 
                                     'ROOT_pretty_name': cr_def.ROOT_pretty_name(), 
                                     'overflow'   : cr_def.overflow(),
                                     'selection': self.get_selection("CR", plot_name),
                                     'channel'  : self.get_channel(plot_name) }


    def write(self, directory):
        yaml.add_representer(str, utils.yaml_latex_representer)
        with open(os.path.join(directory, self._config_name), 'w') as f:
            yaml.dump(self._meas_dict, f, default_flow_style=False, indent=4)

        plot_dicts = {
            'reco_plots': self._reco_plot_dict,
            'auxiliary_observables': self._aux_dict,
            'cr_plots': self._cr_dict,
        }
        with open(os.path.join( directory, self._plots_name), 'w') as f:
            yaml.dump(plot_dicts, f, default_flow_style=False, indent=4)
