import os
import yaml
import logging
logger = logging.getLogger("ttbb plotter")
from functools import partial

from bamboo import treefunctions as op
from bamboo.analysisutils import forceDefine
from bamboo.scalefactors import get_correction

from baseTtbbPlotter import baseTtbbSkimmer
import definitions as defs
import utils
# FIXME
# For 2017 we need to loop over the electron collection since we can't be sure
# we have at least one, so use different implementation from the one in definitions.py
def eleTriggerDef(TrigObj, HLT, electrons, sample, era, isMC):
    cuts = []
    if '2016UL' in era:
        cuts.append(HLT.Ele27_WPTight_Gsf)
    elif era == '2017UL':
        cuts.append(op.OR(
            op.AND(HLT.Ele32_WPTight_Gsf_L1DoubleEG, op.rng_any(electrons, lambda ele: op.rng_any(TrigObj, lambda obj: op.AND(op.deltaR(obj.p4, ele.p4) < 0.1, obj.filterBits & 1024)))),
            HLT.Ele28_eta2p1_WPTight_Gsf_HT150))
    elif era == '2018UL':
        cuts.append(op.OR(HLT.Ele32_WPTight_Gsf, HLT.Ele28_eta2p1_WPTight_Gsf_HT150))
    if not isMC:
        if era == '2016ULpreVFP' or era == '2016ULpostVFP' or era == '2017UL':
            cuts.append(op.c_bool("SingleElectron" in sample))
        if era == '2018UL':
            cuts.append(op.c_bool("EGamma" in sample)) # only called EGamma in 2018
    return cuts

class syncSkimmer(baseTtbbSkimmer):
    def __init__(self, args):
        super().__init__(args)

    def customizeAnalysisCfg(self, analysisCfg):
        eras = self.args.eras[1]
        samples = {}
        # make sure we use absolute paths as this argument will be used by the worker jobs
        self.args.samples = [ os.path.abspath(p) for p in self.args.samples ]
        for tmpPath in self.args.samples:
            with open(tmpPath) as f_:
                template = yaml.load(f_, Loader=yaml.SafeLoader)
                # remove samples not meant for skimming before we "process" the list
                for smpNm in list(template.keys()):
                    if not template[smpNm].get("for_sync", False):
                        template.pop(smpNm)
                        continue
                    template[smpNm].pop("subprocesses", None)
                samples.update(utils.fillSampleTemplate(template, eras))
        analysisCfg["samples"] = samples
        super().customizeAnalysisCfg(analysisCfg)

    def prepareTree(self, tree, sample=None, sampleCfg=None):
        tree,noSel,be,lumiArgs = super().prepareTree(tree, sample=sample, sampleCfg=sampleCfg)
        era = sampleCfg["era"]

        ##### On-the-fly corrections
        if self.args.roc_corr:
            defs.addMuonRocCor(be, tree._Muon, era, sample, self.isMC(sample))
        isNotWorker = (self.args.distributed != "worker")
        defs.configureJetMETCorrections(tree, era, isNotWorker, self.isMC(sample), be, sample)

        return tree,noSel,be,lumiArgs

    def defineSkimSelection(self, t, noSel, sample=None, sampleCfg=None):
        era = sampleCfg["era"]

        # variables to keep from the input tree
        varsToKeep = {"run": None, "luminosityBlock": None, "event": None}

        if not self.isMC(sample):
            if "2016UL" in era:
                noSel = noSel.refine("forCsv", cut=op.AND(t.run == 274335, t.luminosityBlock >= 90, t.luminosityBlock <= 97))
            elif era == "2017UL":
                noSel = noSel.refine("forCsv", cut=op.AND(t.run == 297503, t.luminosityBlock >= 5, t.luminosityBlock <= 11))
            elif era == "2018UL":
                # noSel = noSel.refine("forCsv", cut=op.OR(t.run == 316187, t.run == 316985))
                # noSel = noSel.refine("forCsv", cut=op.AND(t.run == 316187, t.luminosityBlock == 71))
                noSel = noSel.refine("forCsv", cut=op.AND(t.run == 316060, t.luminosityBlock >= 1, t.luminosityBlock <= 10))
        elif sample.startswith("TTToSemiLeptonic") and self.isMC(sample):
            noSel = noSel.refine("forCsv", cut=t.luminosityBlock <= 40)
        else:
            logger.warning("Could not restrict event range!")

        if self.isMC(sample):
            if self.args.roc_corr:
                forceDefine(t._Muon.calcProd, noSel)
            forceDefine(t._Jet.calcProd, noSel)

        ##### Lepton definition

        muons = op.select(t.Muon, defs.muonDef(era))
        muon = muons[0]
        electrons = op.select(t.Electron, defs.eleDef(era))
        electron = electrons[0]

        vetoMuons = op.select(t.Muon, defs.vetoMuonDef)
        vetoElectrons = op.select(t.Electron, defs.vetoEleDef)
        # vetoElectrons = op.sort(op.select(t.Electron, defs.vetoEleDef), lambda ele: -ele.pt)
        # muon = vetoMuons[0]
        # electron = vetoElectrons[0]

        oneMuSel, oneMuTriggerSel = defs.buildMuonSelections(t, noSel, muons, vetoMuons, electrons, vetoElectrons, sample, era, self.isMC(sample))

        oneEleSel, oneEleTriggerSel = defs.buildElectronSelections(t, noSel, muons, vetoMuons, electrons, vetoElectrons, sample, era, self.isMC(sample))

        oneLepSel = noSel.refine("oneLep", cut=op.OR(oneMuSel.cut, oneEleSel.cut))
        # oneLepSel = noSel.refine("oneLep", cut=op.rng_len(vetoElectrons) >= 1)

        ##### Jet definition

        # NOTE: using original jet collection (from nanoAOD)
        if self.isMC(sample):
            jetColl = t._Jet.orig
        else:
            jetColl = t.Jet
        jets = op.select(jetColl, defs.jetDef)
        cleanedJets = defs.cleanJets(jets, muons, electrons)
        bJets = defs.bTagDef(cleanedJets, era)

        ##### define our variables

        varsToKeep["is_e"] = op.static_cast("UInt_t", oneEleSel.cut)
        varsToKeep["is_mu"] = op.static_cast("UInt_t", oneMuSel.cut)

        varsToKeep["flags"] = op.static_cast("UInt_t", op.AND(*defs.flagDef(t.Flag, era, self.isMC(sample))))
        varsToKeep["trigger_e"] = op.static_cast("UInt_t", op.switch(oneEleSel.cut,
                                                                     op.AND(*eleTriggerDef(t.TrigObj, t.HLT, electrons, sample, era, self.isMC(sample))),
                                                                     op.c_bool(False)))
        varsToKeep["trigger_mu"] = op.static_cast("UInt_t", op.switch(oneMuSel.cut,
                                                                      op.AND(*defs.muonTriggerDef(t.HLT, sample, era, self.isMC(sample))),
                                                                      op.c_bool(False)))

        def getLepV(attr):
            # return op.switch(oneLepSel.cut, getattr(electron, attr), getattr(muon, attr))
            return op.switch(oneEleSel.cut, getattr(electron, attr), getattr(muon, attr))

        # varsToKeep["lepton_pt_uncorr"] = op.switch(oneLepSel.cut, electron.pt/electron.eCorr, muon.pt)
        varsToKeep["lepton_pt_uncorr"] = op.switch(oneEleSel.cut,
                                                   electron.pt/electron.eCorr,
                                                   t._Muon.orig[muon.idx].pt if self.args.roc_corr else op.c_float(-1.))
        varsToKeep["lepton_pt"] = getLepV("pt")
        varsToKeep["lepton_eta"] = getLepV("eta")
        # varsToKeep["lepton_deltaEtaSC"] = op.switch(oneLepSel.cut, electron.deltaEtaSC, op.c_float(1.))
        varsToKeep["lepton_deltaEtaSC"] = op.switch(oneEleSel.cut, electron.deltaEtaSC, op.c_float(-1))
        varsToKeep["lepton_phi"] = getLepV("phi")
        varsToKeep["lepton_abs_dxy"] = op.abs(getLepV("dxy"))
        varsToKeep["lepton_abs_dz"] = op.abs(getLepV("dz"))
        # varsToKeep["lepton_iso"] = op.switch(oneLepSel.cut, electron.pfRelIso03_all, muon.pfRelIso04_all)
        varsToKeep["lepton_iso"] = op.switch(oneEleSel.cut, electron.pfRelIso03_all, muon.pfRelIso04_all)
        # varsToKeep["lepton_id_tight"] = op.static_cast("UInt_t", electron.cutBased == 4)

        def getJetV(n, attr, jetColl=cleanedJets):
            return op.switch(op.rng_len(jetColl) >= n, getattr(jetColl[n-1], attr), op.c_float(-1))

        def getJetVFromFunc(n, func, jetColl=cleanedJets):
            return op.switch(op.rng_len(jetColl) >= n, func(jetColl[n-1]), op.c_float(-1))

        varsToKeep["nJets"] = op.static_cast("UInt_t", op.rng_len(cleanedJets))
        varsToKeep["nBJets_deepJetM"] = op.static_cast("UInt_t", op.rng_len(bJets))

        maxJetN = 4

        for n in range(1,maxJetN + 1):
            varsToKeep[f"jet{n}_pt"] = getJetV(n, "pt")
            varsToKeep[f"jet{n}_eta"] = getJetV(n, "eta")
            varsToKeep[f"jet{n}_phi"] = getJetV(n, "phi")
            varsToKeep[f"jet{n}_deepJet"] = getJetV(n, "btagDeepFlavB")

        varsToKeep["nPV"] = t.PV.npvs

        # varsToKeep["MET_t1_pt"] = t._MET["nominal"].pt
        # varsToKeep["MET_t1_phi"] = t._MET["nominal"].phi
        # corrMET = defs.corrMET(t._MET["nominal"], t.PV, sample, era, self.isMC(sample))
        # varsToKeep["MET_xy_pt"] = corrMET.pt
        # varsToKeep["MET_xy_phi"] = corrMET.phi

        def makeBtagEffs(sel, wp):
            bTagEff_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "scale-factors", "btagEff", f"btagEff_deepJet_{era.replace('UL', '')}.json")
            params = {
                "pt": lambda j: j.pt, "eta": lambda j: j.eta,
                "jetFlavor": lambda j: j.hadronFlavour, "workingPoint": wp
            }
            return get_correction(bTagEff_file, "btagEff", params=params, sel=sel, defineOnFirstUse=False)

        def makeBtagSF(sel, wp):
            get_bTagSF = partial(defs.get_bTagSF_fixWP, era=era, sel=sel, use_nominal_jet_pt=False, defineOnFirstUse=False)
            return lambda j: op.multiSwitch(
                (j.hadronFlavour == 5, get_bTagSF(wp, 5)(j)),
                (j.hadronFlavour == 4, get_bTagSF(wp, 4)(j)),
                get_bTagSF(wp, 0)(j))

        btagSFWeightFun = defs.makeBtagSFWPs(era=era, sel=oneLepSel, wps=["M", "T"], defineOnFirstUse=False)
        btagSFItFitFun = defs.makeBtagSFItFit(era, oneLepSel)

        if self.isMC(sample):

            varsToKeep["btag_weight_fixWP_MT"] = op.rng_product(cleanedJets, btagSFWeightFun)
            varsToKeep["btag_weight_itFit"] = op.rng_product(cleanedJets, btagSFItFitFun)

            bTagEffM = makeBtagEffs(oneLepSel, "M")
            bTagEffT = makeBtagEffs(oneLepSel, "T")
            bTagSFM = makeBtagSF(oneLepSel, "M")
            bTagSFT = makeBtagSF(oneLepSel, "T")
            for n in range(1, maxJetN + 1):
                varsToKeep[f"jet{n}_hadronFlavour"] = getJetV(n, "hadronFlavour")
                varsToKeep[f"jet{n}_btag_eff_fixWP_M"] = getJetVFromFunc(n, bTagEffM)
                varsToKeep[f"jet{n}_btag_eff_fixWP_T"] = getJetVFromFunc(n, bTagEffT)
                varsToKeep[f"jet{n}_btag_SF_fixWP_M"] = getJetVFromFunc(n, bTagSFM)
                varsToKeep[f"jet{n}_btag_SF_fixWP_T"] = getJetVFromFunc(n, bTagSFT)
                varsToKeep[f"jet{n}_btag_weight_fixWP_MT"] = getJetVFromFunc(n, btagSFWeightFun)


            eleRecoSF = defs.getScaleFactor(era, oneLepSel, "electron_reco", defineOnFirstUse=False)
            eleIDSF = defs.getScaleFactor(era, oneLepSel, "electron_ID", defineOnFirstUse=False)
            muonIDSF = defs.getScaleFactor(era, oneLepSel, "muon_ID", defineOnFirstUse=False)
            muonIsoSF = defs.getScaleFactor(era, oneLepSel, "muon_iso", defineOnFirstUse=False)
            varsToKeep["lepton_SF"] = op.switch(oneEleSel.cut, eleRecoSF(electron) * eleIDSF(electron), muonIDSF(muon) * muonIsoSF(muon))

            eleTriggerSF = defs.getScaleFactor(era, oneLepSel, "electron_trigger", defineOnFirstUse=False)
            muonTriggerSF = defs.getScaleFactor(era, oneLepSel, "muon_trigger", defineOnFirstUse=False)
            varsToKeep["trigger_SF"] = op.switch(oneEleSel.cut, eleTriggerSF(electron) * (0.991 if era == "2017UL" else 1.), muonTriggerSF(muon))

            varsToKeep["L1_prefire_weight"] = defs.getL1PrefiringSystematic(t)

            varsToKeep["pu_nTrueInt"] = t.Pileup.nTrueInt
            varsToKeep["pu_weight"] = defs.makePUWeight(t, era, oneLepSel)

            # varsToKeep["MET_smeared_pt"] = t._MET["jer"].pt
            # varsToKeep["MET_smeared_phi"] = t._MET["jer"].phi
        else:
            for var in ["lepton_SF", "trigger_SF", "L1_prefire_weight", "pu_nTrueInt", "pu_weight"]:
                varsToKeep[var] = op.c_float(-1)
            for n in range(1,maxJetN + 1):
                for var in [f"jet{n}_hadronFlavour",
                            f"jet{n}_btag_eff_fixWP_M",
                            f"jet{n}_btag_eff_fixWP_T",
                            f"jet{n}_btag_SF_fixWP_M",
                            f"jet{n}_btag_SF_fixWP_T",
                            f"jet{n}_btag_weight_fixWP_MT"]:
                    varsToKeep[var] = op.c_float(-1)

        return oneLepSel, varsToKeep
