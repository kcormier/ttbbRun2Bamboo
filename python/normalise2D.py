#!/usr/bin/env python
import argparse
import yaml
import os
import HistogramTools as HT
import ROOT as R
import numpy as np
R.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', nargs='+', help='input file(s)')
parser.add_argument('-c', '--config', help='yml file for cross sections')
parser.add_argument('-o', '--output', help='output file name (only works if single input file)')
parser.add_argument('-f', '--folder', help='output folder')
args = parser.parse_args()

for inPath in args.input:

    inFile = HT.openFileAndGet(inPath)
    hists = dict()
    HT.readRecursiveDirContent(hists, inFile, resetDir=False)

#Import and rebin histos
    leppt = hists['compare_extrajets_1lep_6j_4b_extra_jet_pT_comparison']
    leppt.Rebin2D(3,3)
    lepptmin = 30
    lepptmax = 730
    lepptbins = 40
    lepptevents = 1000 #Minimum number of events per bin (excluding the last one)
    lepM = hists['compare_extrajets_1lep_6j_4b_extra_jet_M_comparison']
    lepMmin = 30
    lepMmax = 530
    lepMbins = 120
    lepMevents = 1000
    lepDR = hists['compare_extrajets_1lep_6j_4b_extra_jet_DR_comparison']
    lepDRmin = 0
    lepDRmax = 4
    lepDRbins = 100
    lepDRevents = 1000

#Leppt
#Find the new bins
    sum = 0
    binnum = 1
    bins = []
    final_bins = []
    bins.append(lepptmin) #Starting point
    for gen in range(lepptbins + 1):
        for reco in range(lepptbins + 1):
            sum += leppt.GetBinContent(gen,reco)
        if sum >= lepptevents and gen < lepptbins:
            bins.append(gen*(lepptmax-lepptmin)/lepptbins+lepptmin)
            final_bins.append(gen)
            binnum += 1
            sum = 0
        if sum < lepptevents and gen < lepptbins:
            continue
        if gen == lepptbins:
            bins.append(gen*(lepptmax-lepptmin)/lepptbins+lepptmin)
            final_bins.append(gen)
            binnum += 1
    leppt_new = R.TH2D("norm_lep_pT","Normalized Lepton pT;Gen pT (GeV);Reco pt (GeV)", binnum - 1, np.array(bins), lepptbins -1, lepptmin, lepptmax)

#Fill normalised histo
    sum = 0
    newgen = 0
    for gen in range(lepptbins + 1):
        for reco in range(lepptbins + 1):
            sum += leppt.GetBinContent(gen,reco)
        if sum >= lepptevents:
            if newgen != binnum - 2:
                if gen < final_bins[newgen + 1] and gen >= final_bins[newgen]:
                    for reco in range(lepptbins + 1):
                        for genin in range(lepptbins + 1):
                           if genin < final_bins[newgen + 1] and genin >= final_bins[newgen]:
                               leppt_new.Fill(bins[newgen],(reco -1)*(lepptmax-lepptmin)/lepptbins+lepptmin,leppt.GetBinContent(genin,reco)/sum)
                    newgen += 1
            else:
                for reco in range(lepptbins + 1):
                    if sum != 0:
                        for genin in range(lepptbins + 1):
                            if genin >= final_bins[newgen]:
                                leppt_new.Fill(bins[newgen],(reco -1)*(lepptmax-lepptmin)/lepptbins+lepptmin,leppt.GetBinContent(genin,reco)/sum)
                    if sum == 0:
                        leppt_new.Fill(bins[newgen],(reco -1)*(lepptmax-lepptmin)/lepptbins+lepptmin,0)
            sum = 0

#LepM
#Find the new bins
    sum = 0
    binnum = 1
    bins = []
    final_bins = []
    bins.append(lepMmin) #Starting point
    for gen in range(lepMbins + 1):
        for reco in range(lepMbins + 1):
            sum += lepM.GetBinContent(gen,reco)
        if sum >= lepMevents and gen < lepMbins:
            bins.append(gen*(lepMmax-lepMmin)/lepMbins+lepMmin)
            final_bins.append(gen)
            binnum += 1
            sum = 0
        if sum < lepMevents and gen < lepMbins:
            continue
        if gen == lepMbins:
            bins.append(gen*(lepMmax-lepMmin)/lepMbins+lepMmin)
            final_bins.append(gen)
            binnum += 1
    lepM_new = R.TH2D("norm_lep_M","Normalized Lepton M;Gen M (GeV);Reco M (GeV)", binnum - 1, np.array(bins), lepMbins -1, lepMmin -1, lepMmax)

#Fill normalised histo
    sum = 0
    newgen = 0
    for gen in range(lepMbins + 1):
        for reco in range(lepMbins + 1):
            sum += lepM.GetBinContent(gen,reco)
        if sum >= lepMevents:
            if newgen != binnum - 2:
                if gen < final_bins[newgen + 1] and gen >= final_bins[newgen]:
                    for reco in range(lepMbins + 1):
                        for genin in range(lepMbins + 1):
                            if genin < final_bins[newgen + 1] and genin >= final_bins[newgen]:
                                lepM_new.Fill(bins[newgen],(reco -1)*(lepMmax-lepMmin)/lepMbins+lepMmin,lepM.GetBinContent(genin,reco)/sum)
                    newgen += 1
            else:
                for reco in range(lepMbins + 1):
                    if sum != 0:
                        for genin in range(lepMbins + 1):
                            if genin >= final_bins[newgen]:
                                lepM_new.Fill(bins[newgen],(reco -1)*(lepMmax-lepMmin)/lepMbins+lepMmin,lepM.GetBinContent(genin,reco)/sum)
                    if sum == 0:
                        lepM_new.Fill(bins[newgen],(reco -1)*(lepMmax-lepMmin)/lepMbins+lepMmin,0)
            sum = 0

#LepDR
#Find the new bins
    sum = 0
    binnum = 1
    bins = []
    final_bins = []
    bins.append(lepDRmin) #Starting point
    for gen in range(lepDRbins + 1):
        for reco in range(lepDRbins + 1):
            sum += lepDR.GetBinContent(gen,reco)
        if sum >= lepDRevents and gen < lepDRbins:
            bins.append(gen*(lepDRmax-lepDRmin)/lepDRbins+lepDRmin)
            final_bins.append(gen)
            binnum += 1
            sum = 0
        if sum < lepDRevents and gen < lepDRbins:
            continue
        if gen == lepDRbins:
            bins.append(gen*(lepDRmax-lepDRmin)/lepDRbins+lepDRmin)
            final_bins.append(gen)
            binnum += 1
    lepDR_new = R.TH2D("norm_lep_DR","Normalized Lepton DR;Gen DR;Reco DR", binnum - 1, np.array(bins), lepDRbins -1, lepDRmin, lepDRmax)

#Fill normalised histo
    sum = 0
    newgen = 0
    for gen in range(lepDRbins + 1):
        for reco in range(lepDRbins + 1):
            sum += lepDR.GetBinContent(gen,reco)
        if sum >= lepDRevents:
            if newgen != binnum - 2:
                if gen < final_bins[newgen + 1] and gen >= final_bins[newgen]:
                    for reco in range(lepDRbins + 1):
                        for genin in range(lepDRbins + 1):
                            if genin < final_bins[newgen + 1] and genin >= final_bins[newgen]:
                                lepDR_new.Fill(bins[newgen],(reco -1)*(lepDRmax-lepDRmin)/lepDRbins+lepDRmin,lepDR.GetBinContent(genin,reco)/sum)
                    newgen += 1
            else:
                for reco in range(lepDRbins + 1):
                    if sum != 0:
                        for genin in range(lepDRbins + 1):
                            if genin >= final_bins[newgen]:
                                lepDR_new.Fill(bins[newgen],(reco -1)*(lepDRmax-lepDRmin)/lepDRbins+lepDRmin,lepDR.GetBinContent(genin,reco)/sum)
                    if sum == 0:
                        lepDR_new.Fill(bins[newgen],(reco -1)*(lepDRmax-lepDRmin)/lepDRbins+lepDRmin,0)
            sum = 0

 #   sum = 0
 #   for gen in range(101):
 #       for reco in range(101):
 #           sum += lepDR.GetBinContent(gen,reco)
 #       for reco in range(101):
 #           if sum > 0:
 #               lepDR_new.SetBinContent(gen,reco,lepDR.GetBinContent(gen,reco)/sum)
 #           else:
 #               lepDR_new.SetBinContent(gen,reco,0)
 #       sum = 0

    if args.output and len(args.input) == 1 and args.folder == 0:
        outPath = args.output
    else:
        if not os.path.exists(args.folder):
            os.makedirs(args.folder)
        outPath = os.path.join(args.folder, os.path.basename(args.output))
    print(f"Writing produced histograms to {outPath}")
    outFile = R.TFile(outPath, "recreate")

#Prepare histos for saving
    lepptcanvlog = R.TCanvas() 
    lepptcanvlog.SetLogz()
    R.gStyle.SetOptStat(0)
    leppt_new.Draw("colz")
    leppt_new.Write()
    lepptcanv = R.TCanvas() 
    R.gStyle.SetOptStat(0)
    leppt_new.Draw("colz")
    leppt_new.Write()
    lepMcanvlog = R.TCanvas() 
    lepMcanvlog.SetLogz()
    R.gStyle.SetOptStat(0)
    lepM_new.Draw("colz")
    lepM_new.Write()
    lepMcanv = R.TCanvas() 
    R.gStyle.SetOptStat(0)
    lepM_new.Draw("colz")
    lepM_new.Write()
    lepDRcanvlog = R.TCanvas() 
    lepDRcanvlog.SetLogz()
    R.gStyle.SetOptStat(0)
    lepDR_new.Draw("colz")
    lepDR_new.Write()
    lepDRcanv = R.TCanvas() 
    R.gStyle.SetOptStat(0)
    lepDR_new.Draw("colz")
    lepDR_new.Write()

#Save histos
    lepptcanvlog.SaveAs(os.path.join(args.folder,'Normalised lep pT_log.pdf'),'pdf')
    lepptcanv.SaveAs(os.path.join(args.folder,'Normalised lep pT.pdf'),'pdf')
    lepMcanvlog.SaveAs(os.path.join(args.folder,'Normalised lep M_log.pdf'),'pdf')
    lepMcanv.SaveAs(os.path.join(args.folder,'Normalised lep M.pdf'),'pdf')
    lepDRcanvlog.SaveAs(os.path.join(args.folder,'Normalised lep DR_log.pdf'),'pdf')
    lepDRcanv.SaveAs(os.path.join(args.folder,'Normalised lep DR.pdf'),'pdf')

print("Done")
