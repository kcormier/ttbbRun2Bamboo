import os

import logging
logger = logging.getLogger("ttbb plotter")

from bamboo.plots import Plot, SummedPlot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo.plots import VariableBinning as VarBin
from bamboo import treefunctions as op
from bamboo.root import gbl as ROOT

import numpy as np

import definitions as defs
import utils
import controlPlotDefinition as cp
import HistogramTools as HT

import genDefinitions as genDefs
import unfoldingDefinitions as unfDefs
import unfoldingConfigWriter as cw

from genBaseTtbbPlotter import genBaseTtbbPlotter
from recoBaseTtbbPlotter import recoBaseTtbbPlotter

class unfoldingPlotter(recoBaseTtbbPlotter, genBaseTtbbPlotter):
    """Produce Histograms used as inputs to the Unfolding."""
    def __init__(self, args):
        super().__init__(args)
        self.configWriter = cw.ConfigWriter()

    def addArgs(self, parser):
        super().addArgs(parser)
        parser.add_argument("--add-cr", action="store_true", help="Produce control region histograms")

    def prepareTree(self, tree, sample=None, sampleCfg=None):
        # take care to not already split the ttbar categories,
        # because we need to be careful with the signal definition
        return super().prepareTree(tree, sample=sample, sampleCfg=sampleCfg, splitTT=False)

    def defineCollections(self, t, noSel, sample=None, sampleCfg=None):
        do_migrations = sampleCfg.get("is_signal", False)
        do_only_gen = sampleCfg.get("gen_only", False)

        jet_collections = unfDefs.MeasurementCollection( )
        bJetPair_collections = unfDefs.MeasurementCollection( )
        bJet_collections = unfDefs.MeasurementCollection( )
        lightJet_collections = unfDefs.MeasurementCollection( )
        minDRbbPair_collections = unfDefs.MeasurementCollection( )
        maxMbbPair_collections = unfDefs.MeasurementCollection( )
        nonWLightJet_collections = unfDefs.MeasurementCollection( )
        softB_collections = unfDefs.MeasurementCollection( )

        if not do_only_gen:
            recoBaseTtbbPlotter.defineObjects(self, t, noSel, sample, sampleCfg)
            # Find pairs of extra bjets
            bJetCouples = op.combine(self.bJetsM, N=2)
            minDRbbPair = op.rng_min_element_by(bJetCouples, lambda bcoup: op.deltaR(bcoup[0].p4, bcoup[1].p4))
            maxMbbPair  = op.rng_max_element_by(bJetCouples, lambda pair: op.invariant_mass(pair[0].p4, pair[1].p4))
            softestBJet = op.sort(self.bJetsM, lambda jet : jet.pt)

            # Extra light jets -> reconstruct hadronic W
            matchingChi2 = lambda w: op.pow(op.invariant_mass(w) - 82.8, 2.)
            lightJetPairs = op.combine(self.lightJetsM, N=2)
            wCand = op.rng_min_element_by(lightJetPairs, lambda pair: matchingChi2(pair[0].p4 + pair[1].p4))
            lightJets_notW = op.select(self.lightJetsM, lambda j: op.NOT(op.OR(j == wCand[0], j == wCand[1])))

            jet_collections.reco_collection = self.cleanedJets
            bJetPair_collections.reco_collection = bJetCouples
            bJet_collections.reco_collection = self.bJetsM
            lightJet_collections.reco_collection = self.lightJetsM
            minDRbbPair_collections.reco_collection = minDRbbPair
            maxMbbPair_collections.reco_collection = maxMbbPair
            nonWLightJet_collections.reco_collection = lightJets_notW
            softB_collections.reco_collection = softestBJet

        if do_migrations or do_only_gen:

            genBaseTtbbPlotter.defineObjects(self, t, noSel, sample, sampleCfg)

            genBJetCouples = op.combine(self.genBJets, N=2)
            genMinDRbbPair = op.rng_min_element_by(genBJetCouples, lambda bcoup: op.deltaR(bcoup[0].p4, bcoup[1].p4))
            genMaxMbbPair = op.rng_max_element_by(genBJetCouples, lambda pair: op.invariant_mass(pair[0].p4, pair[1].p4))
            genSoftestBJet = op.sort(self.genBJets, lambda jet : jet.pt)
            genMatchingChi2 = lambda w: op.pow(op.invariant_mass(w) - 79.6, 2.)
            genLightJetPairs = op.combine(self.genLightJets, N=2)
            genWCand = op.rng_min_element_by(genLightJetPairs, lambda pair: genMatchingChi2(pair[0].p4 + pair[1].p4))
            genLightJets_notW = op.select(self.genLightJets, lambda j: op.NOT(op.OR(j == genWCand[0], j == genWCand[1])))

            jet_collections.gen_collection = self.genCleanedJets
            lightJet_collections.gen_collection = self.genLightJets
            bJetPair_collections.gen_collection = genBJetCouples
            bJet_collections.gen_collection = self.genBJets
            minDRbbPair_collections.gen_collection = genMinDRbbPair
            maxMbbPair_collections.gen_collection = genMaxMbbPair
            nonWLightJet_collections.gen_collection = genLightJets_notW
            softB_collections.gen_collection = genSoftestBJet

        return (jet_collections, \
                bJet_collections, \
                bJetPair_collections, \
                lightJet_collections, \
                minDRbbPair_collections, \
                maxMbbPair_collections, \
                nonWLightJet_collections, \
                softB_collections)

    def defineSelections(self, t, noSel, sample=None, sampleCfg=None):
        #define measurement phase spaces  (njets, nb, nlightjets)
        selections = [
            { "gen": (5, 3, None), "reco": (5, 3, None), "cr": (5, 2, None) },
            { "gen": (6, 4, None), "reco": (6, 4, None), "cr": (6, 2, None) },
            { "gen": (6, 3, 3), "reco": (6, 3, 3), "cr": (6, 2, 3) },
            { "gen": (7, 4, 3), "reco": (7, 4, 3), "cr": (7, 2, 3) },
        ]
        if self.args.add_cr:
            logger.info("Will produce control region histograms")
        else:
            for s in selections:
                s.pop("cr")

        do_migrations = sampleCfg.get("is_signal", False)
        do_only_gen = sampleCfg.get("gen_only", False)
        ttSubProc = sampleCfg.get("subprocess", None)

        if not do_only_gen:
            ##### Muon and electron selection, exclusive!
            oneMuTriggerSel, oneEleTriggerSel = recoBaseTtbbPlotter.defineBaseSelections(self, t, noSel, sample, sampleCfg)

        # Muon and electron selection (exclusive!), merged
        # The gen-only selection will also be used for ttjj and ttC,
        # to make sure there is no overlap with the signal,
        # so we must define it not only for the signal
        if ttSubProc or do_migrations or do_only_gen:
            genOnlyOneLepSel = genBaseTtbbPlotter.defineBaseSelections(self, t, noSel, sample, sampleCfg, prefix="genLevel_")

        all_selections = [] # list of MeasurementSelection
        for jet_sel in selections:
            gen_nJet,gen_nB,gen_lJet = jet_sel["gen"]
            reco_numbers = [ (jet_sel["reco"], "reco") ]

            this_selection = unfDefs.MeasurementSelection()
            if "cr" in jet_sel:
                reco_numbers += [ (jet_sel["cr"], "CR") ]
                this_selection.metadata['has_CR'] = True

            if ttSubProc or do_migrations or do_only_gen:
                genJetCut = op.rng_len(self.genCleanedJets) >= gen_nJet
                genBJetCut = op.rng_len(self.genBJets) >= gen_nB
                if gen_lJet is not None:
                    genLightJetCut =  op.rng_len(self.genLightJets) >= gen_lJet
                    gen_only_sel_name = f'genLevel_lepton_{gen_nJet}j_{gen_nB}b_{gen_lJet}lj'
                    genOnlySel = genOnlyOneLepSel.refine(gen_only_sel_name, cut=op.AND(genJetCut, genBJetCut, genLightJetCut) )
                else:
                    gen_only_sel_name = f'genLevel_lepton_{gen_nJet}j_{gen_nB}b'
                    genOnlySel = genOnlyOneLepSel.refine(gen_only_sel_name, cut=op.AND(genJetCut, genBJetCut) )
            if do_migrations or do_only_gen:
                this_selection.gen_selection = genOnlySel

            if gen_lJet is not None:
                this_selection.metadata['obs_tag'] =  f'_{gen_nJet}j_{gen_nB}b_{gen_lJet}lj'
            else:
                this_selection.metadata['obs_tag'] =  f'_{gen_nJet}j_{gen_nB}b'
            for typ,(nJet,nB,_) in jet_sel.items():
                this_selection.metadata[f'{typ}_nB'] = nB
                this_selection.metadata[f'{typ}_nJet'] = nJet
            if not do_only_gen:
                for trigger_sel,lep,lep_name in [(oneMuTriggerSel, self.muon, "1mu"), (oneEleTriggerSel, self.electron, "1ele")]:
                    for (reco_nJet,reco_nB,reco_lJet),reco_tag in reco_numbers:
                        jetSel = op.rng_len(self.cleanedJets) >= reco_nJet
                        bTagSel = op.rng_len(self.bJetsM) >= reco_nB
                        # make sure the selection we'll actually use in the end has the correct name
                        tempTag = "_temp" if ttSubProc else ""
                        if reco_lJet is not None:
                            lightJetSel = op.rng_len(self.lightJetsM) >= reco_lJet
                            reco_sel_name = f'{reco_tag}_{lep_name}_{reco_nJet}j_{reco_nB}b_{reco_lJet}lj'
                            recoLeptonJetAndBSel = trigger_sel.refine(reco_sel_name + tempTag, cut=op.AND(jetSel, bTagSel, lightJetSel), weight=self.bTagWeight)
                        else:
                            reco_sel_name = f'{reco_tag}_{lep_name}_{reco_nJet}j_{reco_nB}b'
                            recoLeptonJetAndBSel = trigger_sel.refine(reco_sel_name + tempTag, cut=op.AND(jetSel, bTagSel), weight=self.bTagWeight)

                        # for ttbar, introduce here the ttbarCategorizer cut:
                        # - for ttB, do (ttB || gen)
                        # - for ttjj/ttC, do (ttX && !gen)
                        if ttSubProc:
                            ttCatCut = utils.ttJetFlavCuts(ttSubProc, t)
                            if do_migrations:  # ttB
                                ttCatCut = op.OR(ttCatCut, genOnlySel.cut)
                            else:  # ttjj/ttC
                                ttCatCut = op.AND(ttCatCut, op.NOT(genOnlySel.cut))
                            recoLeptonJetAndBSelWithTTCat = recoLeptonJetAndBSel.refine(reco_sel_name, cut=ttCatCut)
                            this_selection.reco_selections += [ recoLeptonJetAndBSelWithTTCat ]
                        else:
                            this_selection.reco_selections += [ recoLeptonJetAndBSel ]

                        if do_migrations:
                            genOneLepSelAfterReco = genBaseTtbbPlotter.defineBaseSelections(self, t, recoLeptonJetAndBSel, sample, sampleCfg, tag=f'_{recoLeptonJetAndBSel.name}')

                            genJetCut =  op.rng_len(self.genCleanedJets) >= gen_nJet
                            genBJetCut =  op.rng_len(self.genBJets) >= gen_nB
                            if gen_lJet is not None:
                                genLightJetCut =  op.rng_len(self.genLightJets) >= gen_lJet
                                gen_and_reco_sel_name = f'lepton_{gen_nJet}j_{gen_nB}b_{gen_lJet}lj_{recoLeptonJetAndBSel.name}'
                                genAndRecoSel = genOneLepSelAfterReco.refine(gen_and_reco_sel_name, cut=op.AND(genJetCut, genBJetCut, genLightJetCut) )
                            else:
                                gen_and_reco_sel_name = f'lepton_{gen_nJet}j_{gen_nB}b_{recoLeptonJetAndBSel.name}'
                                genAndRecoSel = genOneLepSelAfterReco.refine(gen_and_reco_sel_name, cut=op.AND(genJetCut, genBJetCut) )
                            this_selection.gen_and_reco_selections += [ genAndRecoSel ]

            all_selections += [this_selection]

        return all_selections

    def definePlots(self, t, noSel, sample=None, sampleCfg=None):
        era = sampleCfg["era"]
        plots = []

        jet_collections, \
                bJet_collections, \
                bJetPair_collections, \
                lightJet_collections, \
                minDRbbPair_collections, \
                maxMbbPair_collections, \
                nonWLightJet_collections, \
                softB_collections = \
                self.defineCollections(t, noSel, sample=sample, sampleCfg=sampleCfg)
        all_selections = self.defineSelections(t, noSel, sample, sampleCfg)

        for selection in all_selections:
            gen_nB = selection.metadata['gen_nB']
            gen_nJet = selection.metadata['gen_nJet']
            reco_nB = selection.metadata['reco_nB']
            obs_tag = selection.metadata['obs_tag']  # "_nGenJet_nGenB[_nGenLight]"
            obs_kwargs = { 'tag': obs_tag }

            aux = None
            cr_def = None
            do_only_gen = sampleCfg.get("gen_only", False)
            if not do_only_gen:
                if obs_tag in ["_5j_3b", "_6j_3b_3lj"]:
                    aux_bins = VarBin([0, 2, 3, 6])
                elif obs_tag in ["_6j_4b", "_7j_4b_3lj"]:
                    aux_bins = VarBin([0, 3, 6])
                aux = unfDefs.NBTaggedJets(collection=self.cleanedJets, working_point='T',
                                           tagger=self.btagger, era=era,
                                           binning=aux_bins, overflow=True, tag=obs_tag)

                if selection.metadata.get("has_CR", False):
                    cr_nB = selection.metadata['cr_nB']
                    cr_def = unfDefs.NBTaggedJets(collection=self.cleanedJets, working_point='M',
                                                  tagger=self.btagger, era=era,
                                                  binning=EqBin(reco_nB - cr_nB, cr_nB, reco_nB), overflow=False, tag="")
                    for sel in selection.reco_selections:
                        if "CR" not in sel.name:
                            continue
                        cr_plot, cr_plot_name = unfDefs.make_plot(cr_def, sel, "reco")
                        plots.append(cr_plot)
                        self.configWriter.add_cr_entry(cr_plot_name, cr_def)

            # Light Jet Phase Spaces (6J3B/7J4B)
            if (gen_nJet == 6 and gen_nB == 3) or (gen_nJet == 7 and gen_nB == 4):
                plots += unfDefs.addAllMeasurements([unfDefs.lightJet_Ht],
                                                    selection, lightJet_collections,
                                                    obs_kwargs=obs_kwargs, aux=aux, cr_def=cr_def, config=self.configWriter)
                plots += unfDefs.addAllMeasurements([unfDefs.extraLightJet_Pt],
                                                    selection, nonWLightJet_collections,
                                                    obs_kwargs=obs_kwargs, aux=aux, cr_def=cr_def, config=self.configWriter)
                plots += unfDefs.addAllMeasurements([unfDefs.Jet_dPhi],
                                                    selection, nonWLightJet_collections, softB_collections,
                                                    obs_kwargs=obs_kwargs, aux=aux, cr_def=cr_def, config=self.configWriter)

            # Primary Phase Spaces
            elif (gen_nJet == 5 and gen_nB == 3) or (gen_nJet == 6 and gen_nB == 4):
                plots += unfDefs.addAllMeasurements([unfDefs.Njet],
                                                    selection, jet_collections,
                                                    obs_kwargs=obs_kwargs | {'nMin': gen_nJet}, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)
                # b jet eta only for 3rd b jet (5j3b) or 3rd and 4th b jets (6j4b)
                plots += unfDefs.addAllMeasurements([unfDefs.Bjet_AbsEta],
                                                    selection, bJet_collections,
                                                    obs_kwargs=obs_kwargs | {"minIndex": 2, "maxIndex": gen_nB - 1}, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)
            if gen_nJet == 5 and gen_nB == 3:
                plots += unfDefs.addAllMeasurements([unfDefs.Jet_Ht],
                                                    selection, jet_collections,
                                                    obs_kwargs=obs_kwargs, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)
                plots += unfDefs.addAllMeasurements([unfDefs.bJet_Ht_5j_3b],
                                                    selection, bJet_collections,
                                                    obs_kwargs=obs_kwargs, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)
                # b jet pt only for 3rd b jet
                plots += unfDefs.addAllMeasurements([unfDefs.Bjet_Pt_5j_3b],
                                                    selection, bJet_collections,
                                                    obs_kwargs=obs_kwargs | {"minIndex": 2, "maxIndex": 2}, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)
                # number of b jets, only in 5j3b
                plots += unfDefs.addAllMeasurements([unfDefs.NBTaggedJets],
                                                    selection, jet_collections,
                                                    obs_kwargs=obs_kwargs | {
                                                        "working_point": "M",
                                                        "era": era,
                                                        "tagger": self.btagger if not do_only_gen else None,
                                                        "binning": EqBin(2, 3, 5),
                                                        "overflow": True
                                                    },
                                                    aux=aux, cr_def=cr_def, config=self.configWriter)

            if gen_nJet == 6 and gen_nB == 4:
                plots += unfDefs.addAllMeasurements([unfDefs.Jet_Ht],
                                                    selection, jet_collections,
                                                    obs_kwargs=obs_kwargs, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)
                plots += unfDefs.addAllMeasurements([unfDefs.bJet_Ht_6j_4b],
                                                    selection, bJet_collections,
                                                    obs_kwargs=obs_kwargs, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)
                # b jet pt only for 3rd and 4th b jet
                plots += unfDefs.addAllMeasurements([unfDefs.Bjet_Pt_6j_4b],
                                                    selection, bJet_collections,
                                                    obs_kwargs=obs_kwargs | {"minIndex": 2, "maxIndex": 3}, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)

                extra_jet_observables           = [ unfDefs.Extra_Jet_Pt, unfDefs.Extra_Jet_AbsEta,
                                                   unfDefs.Extra_Jet_dR, unfDefs.Extra_Jet_Sum_Pt,
                                                   unfDefs.Extra_Jet_Sum_AbsEta, unfDefs.Extra_Jet_Mass ]

                # extra jet pt and eta: for 1st and 2nd jet in the pair (duh)
                plots += unfDefs.addAllMeasurements(extra_jet_observables,
                                                    selection, minDRbbPair_collections,
                                                    obs_kwargs=obs_kwargs | {"maxIndex": 1}, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)
                plots += unfDefs.addAllMeasurements([unfDefs.Max_Mbb], selection,
                                                    maxMbbPair_collections,
                                                    obs_kwargs=obs_kwargs, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)
                plots += unfDefs.addAllMeasurements([unfDefs.Average_DR], selection,
                                                    bJetPair_collections,
                                                    obs_kwargs=obs_kwargs, aux=aux,
                                                    cr_def=cr_def, config=self.configWriter)

        return plots

    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None):
        super().postProcess(taskList, config, workdir, resultsdir)
        self.configWriter.write(resultsdir)
