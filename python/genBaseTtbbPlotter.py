import logging
logger = logging.getLogger("ttbb plotter")

from bamboo import treefunctions as op
from bamboo.root import gbl as ROOT

import genDefinitions as genDefs
import utils
from baseTtbbPlotter import baseTtbbPlotter

class genBaseTtbbPlotter(baseTtbbPlotter):
    """"""
    def __init__(self, args):
        super().__init__(args)

    def defineBaseSelections(self, tree, noSel, sample, sampleCfg, **kwargs):
        return genDefs.buildLeptonSelection(noSel, self.genMuons, self.genVetoMuons, self.genElectrons, self.genVetoElectrons, **kwargs)

    def defineObjects(self, tree, noSel, sample=None, sampleCfg=None, **kwargs):
        super().defineObjects(tree, noSel, sample, sampleCfg, **kwargs)

        if not self.isMC(sample):
            return

        era = sampleCfg["era"]

        ##### Lepton definition
        self.genMuons = op.select(tree.GenDressedLepton, genDefs.muonDef)
        self.genMuon = self.genMuons[0]
        self.genElectrons = op.select(tree.GenDressedLepton, genDefs.eleDef)
        self.genElectron = self.genElectrons[0]

        self.genVetoMuons = op.select(tree.GenDressedLepton, genDefs.vetoMuonDef)
        self.genVetoElectrons = op.select(tree.GenDressedLepton, genDefs.vetoEleDef)

        ##### Jet definition
        self.genJets = op.select(tree.GenJet, genDefs.jetDef)
        self.genCleanedJets = genDefs.cleanJets(self.genJets, self.genMuons, self.genElectrons)

        ##### B jet definition
        self.genBJets = op.select(self.genCleanedJets, lambda jet: jet.hadronFlavour == 5)
        self.genLightJets = op.select(self.genCleanedJets, lambda jet: jet.hadronFlavour != 5)

    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None, **kwargs):
        super().postProcess(taskList, 
                            config=config,
                            workdir=workdir, 
                            resultsdir=resultsdir,
                            **kwargs)
