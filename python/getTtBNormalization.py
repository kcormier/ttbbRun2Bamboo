import os

import logging
logger = logging.getLogger("ttbb plotter")

from bamboo.plots import Plot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo import treefunctions as op

import utils
import HistogramTools as HT
from baseTtbbPlotter import baseTtbbPlotter

class getTtBNormalization(baseTtbbPlotter):
    """"""
    def __init__(self, args):
        super().__init__(args)

    def customizeAnalysisCfg(self, analysisCfg):
        self._loadSampleLists(analysisCfg)
        samples = analysisCfg["samples"]

        # only work with the ttB contribution
        for smp in list(samples.keys()):
            if samples[smp].get("subprocess") != "ttB":
                samples.pop(smp)

        super().customizeAnalysisCfg(analysisCfg)

    def definePlots(s, t, noSel, sample=None, sampleCfg=None):
        plots = []
        plots.append(Plot.make1D("yields", op.c_float(0.), noSel, EqBin(1, 0, 1)))
        return plots

    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None, **kwargs):
        super().postProcess(taskList, 
                            config=config,
                            workdir=workdir, 
                            resultsdir=resultsdir,
                            **kwargs)

        def _find_hists(base_name, ufhandle):
            return map(lambda k: k.split(";")[0],  # careful: uproot keys include cycle number ';1'
                        filter(lambda k: k.startswith(base_name), ufhandle))

        def _getSyst(name):
            spl = name.split("__")
            if len(spl) == 1:
                return ""
            else:
                return spl[1]

        import uproot
        hist_list = []
        for task in taskList:
            if "syst" in task.config:
                continue
            signal_tag = task.config["signal_tag"]
            xs = task.config["cross-section"]
            outfile = os.path.join(resultsdir, task.outputFile)
            tf = HT.openFileAndGet(outfile)
            nomWeights = utils.getSumw(tf, task.config, self.readCounters)
            tf.Close()
            with uproot.open(outfile) as ufhandle:
                hists = _find_hists("yields", ufhandle)
                for h in hists:
                    systVar = _getSyst(h)
                    h = ufhandle[h].values()#.to_hist()
                    sumw = xs * h.sum() / nomWeights
                    hist_list.append({"signal_tag": signal_tag, "syst": systVar, "sumw": sumw})

        import pandas as pd
        df = pd.DataFrame(hist_list)
        results = {}
        for signal_tag, sig_df in df.groupby("signal_tag"):
            nom_df = sig_df[sig_df.syst == ""]
            var_df = sig_df[sig_df.syst != ""]
            nom_sumw = nom_df["sumw"].sum()
            results[signal_tag] = {}
            for systVar, syst_df in var_df.groupby("syst"):
                ratio = syst_df["sumw"].sum() / nom_sumw
                systVar = systVar.replace("up", "Up").replace("down", "Down")
                results[signal_tag][systVar] = float(ratio)

        import yaml
        with open(os.path.join(resultsdir, "ttB_systematics.yaml"), 'w') as f:
            yaml.dump(results, f, default_flow_style=False, indent=4)

